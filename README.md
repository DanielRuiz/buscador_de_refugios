[**Descargar .APK Version Beta**](https://bitbucket.org/DanielRuiz/buscador_de_refugios/src/a4cee33df74eea98484dda7244b07029f771e4a9/app/BuscadorDeRefugiosVersionBetaV2.apk?at=master)
![Alt text](http://www.unicaribe.edu.mx/images/logo-unicaribe.png)
#Universidad del Caribe.
>
>##Proyecto: Localizador de Refugios anticiclónicos en Cancún
>
>![Alt text](http://g4ry.orgfree.com/Screenshot.png)
>
>Desarrolladores:
>>
* Paredes Bernabé Luis Alberto
* Ruiz Cervantes Daniel Eduardo 
* Cabañas Suarez Jesús Azael
>
>Asesor:
>>Ing. Paz Cuevas Francisco
>
>Descripción del Problema:
>>En la actualidad en la ciudad de Cancún, existen refugios anticiclónicos disponibles para la 
población en caso de presentarse un evento ciclónico, dicha lista de refugios se puede obtener 
digitalmente, preguntando directamente en las oficinas de Protección Civil o en su caso, previo al 
evento, la mayoría de las televisoras y emisoras de radio publican la lista, pero para las personas 
es tardado y confuso estar buscando en la lista.
>
>Justificación:
>>En la actualidad es bastante tardado y tedioso hacer la búsqueda en los medios de comunicación 
de entre una lista de aproximadamente 50 refugios y localizar cuál de todos ellos se encuentra 
más cerca de su ubicación, si el usuario desea usar su Smartphone la única opción es descargar el 
archivo .PDF con la lista de refugios, además de que no hay una aplicación que ofrezca esta 
función en particular para la ciudad de Cancún.
>
>Propuesta:
>>De acuerdo al problema antes planteado, el equipo de desarrollo se ha propuesto hacer una 
aplicación que lleve hasta los Smartphone con Sistema Operativo Android, la posibilidad de que la 
aplicación haga el escaneo del lugar donde se encuentre dentro de la ciudad y le entregue una 
lista con la ubicación de los refugios aledaños a su posición.
>>
>>Además de contar con un lector de RSS para tener la posibilidad de ver las últimas noticias 
publicadas por el NOAA así como un acceso directo a la página del NOAA donde podrá visualizarse 
el estado de los eventos cercanos al atlántico y una pequeña imagen satelital de los eventos 
próximos.
>
>Objetivo General:
>>Como objetivo general se desarrollara una aplicación para Android que sea capaz de geo-localizar 
los refugios anticiclónicos a los alrededores del usuario. 
>
>Objetivos Específicos:
>>
* Obtener Ubicación (Android)
* Desplegar mapa desde API Maps
* Mostrar noticias RSS
* Hacer BD con coordenadas de refugios
* Desplegar puntos de ubicación, cercanos a ubicación actual
>
>Arquitectura:
>>La aplicación correrá bajo la plataforma de Sistema Operativo Android en las versiones:
>>>Android 4.0.3 -> 4.4.2


[**Descargar .APK Version Beta**](https://bitbucket.org/DanielRuiz/buscador_de_refugios/src/a4cee33df74eea98484dda7244b07029f771e4a9/app/BuscadorDeRefugiosVersionBetaV2.apk?at=master)