package com.example.daniel.mapsexample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.*;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationListener;

public class Activity1 extends Activity {

    WebView MyBrowser;
    GoogleMap googleMap;
    public DBHelper BD;
    TextView textView;
    static final String DATA_TITLE = "T";
    static final String DATA_LINK  = "L";
    static LinkedList<HashMap<String, String>> data;
    static String feedUrl = "http://www.nhc.noaa.gov/index-at.xml";
    private ProgressDialog progressDialog;
    public Double latitud=21.160555555556;
    public Double longitud=-86.8475;
    public String latString;
    public String longString;
    ArrayList<Refugio> refs =null;

    private final Handler progressHandler = new Handler() {
        @SuppressWarnings("unchecked")
        public void handleMessage(Message msg) {
            if (msg.obj != null) {
                data = (LinkedList<HashMap<String, String>>)msg.obj;
                setData(data);
            }
            progressDialog.dismiss();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity1);
        setTitle("Buscador de Refugios");
       // createMapView();
        //addMarker();

        /***********************BASE DE DATOS**********************************/
        BD = new DBHelper(this);
        BD.open();


        ////////////////mapa
        MyBrowser = (WebView)findViewById(R.id.MyBrowser);
        MyBrowser.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url)
            {
                MyBrowser.loadUrl("javascript:(" +
                                "function () { " +
                                "MyLatLng =new google.maps.LatLng("+latitud.toString()+","+longitud.toString()+");"+
                                "var mapOptions = {"+
                                "zoom: 15, center: MyLatLng };"+
                                "map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);"+
                                "var marker = new google.maps.Marker({position: MyLatLng,map: map,title: 'Posicion Actual'"+
                                " });"+
                                "}" +
                                ")()"
                );
                for (int j=0;j<30;j++){
                    MyBrowser.loadUrl("javascript:(" +
                                    "function () { " +
                                    "MyLatLng =new google.maps.LatLng("+refs.get(j).latitud.toString()+","+refs.get(j).longitud.toString()+");"+
                                    "var image = 'icon.png';"+
                                    "var marker = new google.maps.Marker({position: MyLatLng,map: map,icon:image,title: 'Posicion Actual'});"+
                                    "var contentString = '<p>"+refs.get(j).nombre+"</p>"+refs.get(j).direccion+"';"+
                                    "var infowindow = new google.maps.InfoWindow({content: contentString});"+
                                    "google.maps.event.addListener(marker, 'click', function() {infowindow.open(map,marker);});"+
                                    "}" +
                                    ")()"
                    );}
            }
        });
        MyBrowser.loadUrl("file:///android_asset/ejemplo.html");
        MyBrowser.getSettings().setJavaScriptEnabled(true);

        ////////////////

        //Refugio ref = BD.getRefugio(2);
        //String NAME = ref.latitud.toString();
        // ArrayList<Refugio> refs =null;
        refs = BD.getRefugios();
        //NAME = refs.get(7).nombre;
        //String NAME = BD.getRefugio(2).nombre.toString();
        //refs.add(1,BD.getRefugio(2));
        //refs.get(1);
        //refs.get(2);
        //TextView textView = (TextView)findViewById(R.id.textView);
        //textView.setText(NAME);
       /* TextView textView2 = (TextView)findViewById(R.id.textView2);
        for (int i=1;i<30;i++) {
            textView2.setText(textView2.getText()+">> "+ refs.get(i).nombre+"  Dirección: "+refs.get(i).direccion+" \n");
        }*/


        //////////////Encuentra ubicacion /////////
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        MyLocationListener mlocListener = new MyLocationListener();
        mlocListener.setMainActivity(this);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 0,(LocationListener) mlocListener);

        /******************************LECTOR DE RSS**********************************************
         * personalizamos el evento del click del bot—n de carga
         */
        ////////////////////////////////////////////////////////////////////////////////////////
         ListView lv = (ListView) findViewById(R.id.listViewlstData);
        /**
         * Si el ListView ya contiene datos (es diferente de null) vamos
         * a mostrar un di‡logo preguntando al usuario si est‡ seguro de
         * realizar la carga de nuevo
         */
                if (lv.getAdapter() != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Activity1.this);
                    builder.setMessage("ya ha cargado datos, esta seguro de hacerlo de nuevo?")
                            .setCancelable(false)
                            .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    loadData();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                            .create()
                            .show();

        /**
         * Si el ListView no contiene datos (es null) cargamos con loadData()
         */
                } else {
                    loadData();
                }
        ////////////////////////////////////////////////////////////////////////////////////////
        //ListView lv = (ListView) findViewById(R.id.listViewlstData);
        /**
         * Cuando el usuario haga click en algœn elemento de la lista, lo llevaremos al
         * enlace del elemento a travŽs del navegador.
         */
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> av, View v, int position,
                                    long id) {
                /**
                 * Obtenemos el elemento sobre el que se presion—
                 */
                HashMap<String, String> entry = data.get(position);

                /**
                 * Preparamos el intent ACTION_VIEW y luego iniciamos la actividad (navegador en este caso)
                 */
                Intent browserAction = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(entry.get(DATA_LINK)));
                startActivity(browserAction);
            }
        }
        );
        /*****************************  Termina  LECTOR DE RSS   ***********************************/

        //MyBrowser = (WebView)findViewById(R.id.MyBrowser);
        //MyBrowser.loadUrl("file:///android_asset/ejemplo.html");
        //MyBrowser.getSettings().setJavaScriptEnabled(true);
    }//Cierra OnCreate


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity1, menu);
        return true;
    } //Cierra onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    } //Cierra onOptionsItemSelected

    /**
     * *****************************INICIALIZA EL MAPA***************************************
     */
/*    private void createMapView(){
        try {
            if(null == googleMap){
                googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                        R.id.mapView)).getMap();

                if(null == googleMap) {
                    Toast.makeText(getApplicationContext(),
                            "Error creating map", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (NullPointerException exception){
            Log.e("MapsExample", exception.toString());
        }
    } */ //Cierra CreateMapView

    /**
     * Adds a marker to the map
     */
    private void addMarker(){

        /** Make sure that the map has been initialised **/
        if(null != googleMap){
            googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(0, 0))
                            .title("Marker")
                            .draggable(true)
            );
        }
    }//Cierra addMarker


/************************************* FUNCIONES PARA FEED RSS ******************************/
    /**
     * Funci�n auxiliar que recibe una lista de mapas, y utilizando esta data crea un adaptador
     * para poblar al ListView del dise�o
     * */
    private void setData(LinkedList<HashMap<String, String>> data){
        SimpleAdapter sAdapter = new SimpleAdapter(getApplicationContext(), data,
                android.R.layout.two_line_list_item,
                new String[] { DATA_TITLE, DATA_LINK },
                new int[] { android.R.id.text1, android.R.id.text2 });
        ListView lv = (ListView) findViewById(R.id.listViewlstData);
        lv.setAdapter(sAdapter);
    }

    /**
     * Funci�n auxiliar que inicia la carga de datos, muestra al usuario un di�logo de que
     * se est�n cargando los datos y levanta un thread para lograr la carga.
     */
    private void loadData() {
        progressDialog = ProgressDialog.show(
                Activity1.this,
                "",
                "Por favor espere mientras se cargan los datos...",
                true);

        new Thread(new Runnable(){
            @Override
            public void run() {
                XMLParser parser = new XMLParser(feedUrl);
                Message msg = progressHandler.obtainMessage();
                msg.obj = parser.parse();
                progressHandler.sendMessage(msg);
            }}).start();
    }



    ////////////////////////////INICIA GEOLOCALIZACION/////////////////////////////
    /* Class My Location Listener */
    public class MyLocationListener implements LocationListener {
        Activity mainActivity;

        public Activity getMainActivity() {
            return mainActivity;
        }

        public void setMainActivity(Activity mainActivity) {
            this.mainActivity = mainActivity;
        }

        @Override
        public void onLocationChanged(Location loc) {
            // Este mŽtodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la detecci—n de un cambio de ubicacion
            latitud=loc.getLatitude();
            longitud=loc.getLongitude();

            latString = latitud.toString();
            longString = longitud.toString();
            //mytextView.setText(latString);



            MyBrowser.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url)
                {
                  MyBrowser.loadUrl("javascript:(" +
                                    "function () { " +
                                    "MyLatLng =new google.maps.LatLng("+latString.toString()+","+longString.toString()+");"+
                                    "var mapOptions = {"+
                                    "zoom: 15, center: MyLatLng };"+
                                    "map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);"+
                                    "var marker = new google.maps.Marker({position: MyLatLng,map: map,title: 'Posicion Actual'"+
                                    " });"+
                                    "}" +
                                    ")()"
                    );
                    for (int j=0;j<30;j++){
                        MyBrowser.loadUrl("javascript:(" +
                                        "function () { " +
                                        "MyLatLng =new google.maps.LatLng("+refs.get(j).latitud.toString()+","+refs.get(j).longitud.toString()+");"+
                                        "var image = 'icon.png';"+
                                        "var marker = new google.maps.Marker({position: MyLatLng,map: map,icon:image,title: 'Posicion Actual'});"+
                                        "var contentString = '<p>"+refs.get(j).nombre+"</p>"+refs.get(j).direccion+"';"+
                                        "var infowindow = new google.maps.InfoWindow({content: contentString});"+
                                        "google.maps.event.addListener(marker, 'click', function() {infowindow.open(map,marker);});"+
                                        "}" +
                                        ")()"
                        );}
                }
            });

            MyBrowser.loadUrl("file:///android_asset/ejemplo.html");
            MyBrowser.getSettings().setJavaScriptEnabled(true);

        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este mŽtodo se ejecuta cuando el GPS es desactivado

        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}

    }/* End of Class MyLocationListener */
}//Cierra Activity1
