package com.example.daniel.mapsexample;

/**
 * Created by Daniel on 22/09/2014.
 */
public class Refugio {
    //private Integer id_refugio;
    public String nombre;
    public Integer aulas;
    public Integer espacios;
    public String direccion;
    public Double latitud;
    public Double longitud;

    public Refugio(String nombre, Integer aulas, Integer espacios, String direccion, Double latitud, Double longitud){
    super();
        //this.id_refugio=id_refugio;
        this.nombre=nombre;
        this.aulas=aulas;
        this.espacios=espacios;
        this.direccion=direccion;
        this.latitud=latitud;
        this.longitud=longitud;
    }
    public Refugio(){};
}

